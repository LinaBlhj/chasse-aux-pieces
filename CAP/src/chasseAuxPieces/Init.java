package chasseAuxPieces;

import java.net.Socket;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Initialise la connexion des joueurs
 */

public class Init extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Init() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() {    	
    	ServletContext context = getServletContext();
   
    	ArrayList<HttpSession> listeSessions = new ArrayList<HttpSession>();
    	
    	// Enregistrement dans le contexte de l'application
    	context.setAttribute("listeSessions", listeSessions);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Redirection vers index.html pour inviter l'utilisateur a donner un pseudo
		response.sendRedirect("index.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Gestion des accents
		request.setCharacterEncoding("UTF-8");
		// Session associee a l'utilisateur
		HttpSession session = request.getSession();
		// Recuperer le pseudo envoye via le formulaire
		String pseudo = request.getParameter("pseudo");
	
		if(pseudo.isEmpty() || pseudo.length() > 15) {
			// l'utilisateur n'a pas specifie de pseudo
			response.sendRedirect("index.html");
		}else {
			
			try {
				ServletContext context = getServletContext();
				Socket s = new Socket("localhost", 8888);
				
				// Envoi au serveur
				ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
				String[] listeInfosJoueur = new String[2];
				listeInfosJoueur[0] = "Inscription";
				listeInfosJoueur[1] = pseudo;
				
				out.writeObject(listeInfosJoueur);
				
				// Reponse du serveur
				InputStream in = s.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				String reponse = reader.readLine();
				System.out.println("Reponse serveur (numero joueur) :" + reponse);
				String reponse2 = reader.readLine();
				System.out.println("Reponse serveur (couleur joueur) :" + reponse2);				
				s.close();
				
				if(reponse.equals("InscriptionErreur")) {
					// Le jeu a deja commence, l'utilisateur ne peut plus s'inscrire
					response.sendRedirect("attente.html");
				}else {
									
					// Enregistrement du numero du personnage dans l'attribut "numeroPerso" de l'objet HttpSession associe a la session de l'utilisateur
					session.setAttribute("numeroPerso", reponse);
					session.setAttribute("pseudo", pseudo);
					session.setAttribute("nbPieces", "0");
					String[] couleur= reponse2.split(" ");
					session.setAttribute("couleur", couleur);

					// mise a jour de la liste des sessions
					ArrayList<HttpSession> listeSessions = (ArrayList<HttpSession>)context.getAttribute("listeSessions");
					listeSessions.add(session);
					context.setAttribute("listeSessions", listeSessions);
					
					response.sendRedirect("manette.jsp");
				}
		
			} catch (IOException e) {
				e.printStackTrace();
				response.sendRedirect("index.html");
			}
			
		}
		
	
	}

}
