package chasseAuxPieces;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Admin
 */

public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("index.html");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mdp = request.getParameter("mdp");
		if(mdp.isEmpty()) {
			// l'utilisateur n'a pas specifie de mdp
			response.sendRedirect("admin.html");
		}else if(mdp.equals("K06.hy")) {
		
			ServletContext context = getServletContext();
			ArrayList<HttpSession> listeSessions = (ArrayList<HttpSession>)context.getAttribute("listeSessions");
			System.out.println("Destruction des sessions");
			int cpt = 0;
			for(HttpSession s : listeSessions) {
				if(s != null) {
					s.removeAttribute("numeroPerso");
					s.removeAttribute("pseudo");
					s.removeAttribute("nbPieces");
					s.invalidate();
					cpt++;
				}
			}
			
			listeSessions = new ArrayList<HttpSession>();
			context.setAttribute("listeSessions", listeSessions);
			
			PrintWriter out = response.getWriter();
			out.println("<h1>Destruction des sessions effectue ( "+ cpt + ")</h1>");
		}else {
			response.sendRedirect("index.html");
		}
	}

}
