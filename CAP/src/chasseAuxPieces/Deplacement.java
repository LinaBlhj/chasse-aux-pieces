package chasseAuxPieces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Deplacement
 */

public class Deplacement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Deplacement() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("manette.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		if(session == null ) {
			response.sendRedirect("index.html");
		}else {
			// Recuperer le deplacement envoye via le formulaire
			String depl = request.getParameter("action_joueur");
		
			if(depl == null) {
				System.out.println("depl null");
				response.sendRedirect("manette.jsp");
			}else {
				
				try {
					// Envoi du deplacement au serveur
					Socket s = new Socket("localhost", 8888);
					ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
					String[] infoDepl = new String[3];
					infoDepl[0] = "Deplacement";
					String num = (String)session.getAttribute("numeroPerso");
					infoDepl[1] = num +"";
					infoDepl[2] = depl;
					
					out.writeObject(infoDepl);
					
					// Reponse du serveur
					InputStream in = s.getInputStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(in));
					String reponse = reader.readLine();
					System.out.println("Reponse serveur (nb pieces) :" + reponse);
					
					s.close();				
					if(!reponse.equals("-1")) {
						session.setAttribute("nbPieces", reponse);	
					}
					// Reponse au client, mise a jour du nombre de pieces 
					response.setContentType("text/plain");
					response.getWriter().write(reponse);

				  //response.sendRedirect("manette.jsp");
			
				} catch (IOException e) {
					e.printStackTrace();
					response.sendRedirect("manette.jsp");
				}
			}
		}

	}
}
