<% if(session.getAttribute("pseudo") == null){
	response.sendRedirect("index.html");
} %>

<!Doctype>
<html>
  <head>
     
      <meta charset="utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Manette</title>
  </head>

<style>

  body{
  background-image :url("image/fond.png");
  background-size: cover;
 }


  #direction{
  align-items: center;
  justify-content: center;
  position: relative;
  border-color:black;
  }
   #gauche_droit{
 
  display:flex;
  justify-content: space-around;
  }
  
  #bt_haut{
  width: 0;
  height: 0;
  border-left   : 25px solid transparent;
  border-right  : 25px solid transparent;
  border-bottom : 50px solid #dc6f2d;
  }

    #bt_bas{
  width: 0;
  height: 0;
  border-left  : 25px solid transparent;
  border-right : 25px solid transparent;
  border-top   : 50px solid #dc6f2d;
  }

  #bt_gauche{
  
  width: 0;
  height: 0;
  border-top    : 25px solid transparent;
  border-bottom : 25px solid transparent;
  border-right  : 50px solid brown;
  }
  
  #bt_droite{
  width: 0;
  height: 0;
  border-top    : 25px solid transparent;
  border-bottom : 25px solid transparent;
  border-left   : 50px solid brown; 
  }
  
  


  #haut{
  text-align:center;
  }

  #bas{
  text-align:center;
  }

  .block{
  
  margin-left: auto;
    position: relative;
    top: 0px;
    right: 0px;
 	
  }
  
  
    #id_joueur{
     
	padding-top: 10px;
    display: inline-block;
    position: relative;
    box-sizing: border-box;
  
	
  }
  
	
	#num_pseudo{
 	display: flex;
    align-items: center;
    justify-content: center;
    height: 200px;
    width: 200px;
    border-radius: 4px;
    overflow: hidden;
    background:transparent;
    
   	top: 50%;
  	left: 50%;
  	transform: translate(-50%,-65%);
    position: relative;
    
    color: red;
    font-size: 20px;
    font-family: Arial;
    box-sizing: border-box;
    
      
}
	
	
	
	
	#score{
	
    position: absolute ;
    font-weight: bold;
    color: red;
    font-size: 60px;
    font-family: Arial;
    box-sizing: border-box;
    top: 20%;
  	right: 30%;
    
    
	
	}
	<%String[] couleur=(String[]) session.getAttribute("couleur"); %>
	#carre{
	background-color:rgb(<%=couleur[0] %>,<%=couleur[1] %>,<%=couleur[2] %>);
	height:15px;
	width:15px;
	position:relative;
	left: 5px;
	top: 30px;
	}
	
	.image{
    position: absolute;
   	background-position: center center;

	}
	
   input{
  background:transparent;
  border:transparent;
  }
  
 header{
 display: flex;
 
 }
  
  

</style>


  
  <body>
  <header>

    <div id="id_joueur">

       <img  
      class="image"
      src="image/numero.png"
	    alt="image"
	    title="image"
	    width="200"/>
		<div id="num_pseudo"><%= session.getAttribute("pseudo")%> : <br>numero <%= session.getAttribute("numeroPerso") %><br>couleur : <div id="carre"></div></div>
    </div>
   
   
       <div class="block">
      <img 
	    src="image/bourse.png"
	    alt="image"
	    title="image"
	    width="150"/>
     <div id="score"><%= session.getAttribute("nbPieces") %></div>
    </div>
    
    
     </header>
    <br>
    <br>
    <br>
    
<form method="post" onsubmit="return sendData();">
	<div id="direction">

		<div id="haut">
		   <input id="bt_haut" type="submit" name="action_joueur" value="H" onClick="valeur_depl(value)" />
		</div>

		<div id="gauche_droit">
			 <input id="bt_gauche" type="submit" name="action_joueur" value="G" onClick="valeur_depl(value)"/>
			<input id="bt_droite" type="submit" name="action_joueur" value="D" onClick="valeur_depl(value)"/>
		</div>

		<div id="bas">
			<input id="bt_bas" type="submit" name="action_joueur" value="B" onClick="valeur_depl(value)"/>
		</div>

	 </div>
</form>

  <script src="js/jquery.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/bootstrap.min.js"></script> 

    <script type="text/javascript">
    var action;
    
    // cette fonction est issue de : https://waytolearnx.com/2019/10/soumettre-un-formulaire-sans-recharger-la-page-en-utilisant-ajax-jquery-et-php.html
    function sendData()
    {
      
      $.ajax({
        type: 'post',
        url: 'Deplacement',
        data: {
        	action_joueur:action
        },
        success: function (response) {
        	if(response !='-1'){
            	$('#score').html(response);
        	}
          }
      });
        
      return false;
    }
    
    function valeur_depl(valeur){
    	action = valeur;
    }
    
    </script>
  </body>
  </html>
