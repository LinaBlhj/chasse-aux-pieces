package chasseAuxPieces;

import java.awt.Graphics;


/**
 * Implementation d'un Obstacle
 *
 */
public class Obstacle extends Element {
	
	/*********** Constructeur d'un Obstacle ***************/
	/**
	 * Constructeur d'un Obstacle
	 * 
	 * @param pos
	 * 			la position de l'Obstacle
	 */
	public Obstacle(Position pos) {
		super(pos);
	}
	
	/***********************************************/
	
	/*********** Methodes publiques ***************/
	/**
	 * Redefinition de toString
	 */
	 public String toString() {
		return "" + getPosition() + "Obstacle";
	}
	 
	/**
	 * Affiche un Obstacle
	 * 
	 * @param g
	 * 			le Graphics
	 */
	public void seDessiner(Graphics g) {
		g.setColor(IConfig.COULEUR_OBSTACLE);		
		g.fillRect(getPosition().getX()*IConfig.NB_PIX_CASE, getPosition().getY()*IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE);
		
	}
	/***********************************************/
}
