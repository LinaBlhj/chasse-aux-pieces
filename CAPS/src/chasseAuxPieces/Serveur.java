package chasseAuxPieces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/*
 * Assure la communication avec les joueurs
 */
public class Serveur {
	Panneau panneau;
	
	public Serveur(Panneau p) {panneau = p; }
	
	public void serveurSocket() {
	
			ServerSocket server;
			boolean arret = false;
			try {
				server = new ServerSocket(8888, 1);
				System.out.println("En attente de client...");
				
				while(!arret) {
					Socket s = server.accept();
					try {
						ObjectInputStream in = new ObjectInputStream(s.getInputStream());
						String[] listeInfos = (String[])in.readObject();
						PrintWriter out = new PrintWriter(s.getOutputStream());
						
						if(panneau.getEtatJeu() == 1) {
							// phase d'inscription des joueurs
							if(listeInfos[0].equals("Inscription")) {
								// L'utilisateur vient de la page index.html
								System.out.println("INSCRIPTION :" + listeInfos[1]);	
								int num = panneau.getCarte().ajouterPersonnage(listeInfos[1]);
								String couleurs = panneau.getCarte().getListePersonnages().get(num-1).getCouleur();
								out.println(num+"");
								out.flush();
								out.println(couleurs);
								out.flush();
							}
							else if(listeInfos[0].equals("Deplacement")) {
								// l'utilisateur vient de la page manette.jsp
								System.out.println("DEPLACEMENT : numJoueur : " +  listeInfos[1] + " depl " +  listeInfos[2]);
								int nbPieces = panneau.getCarte().deplacement(Integer.parseInt(listeInfos[1]), listeInfos[2]);
								out.println(nbPieces + "");
								out.flush();
							}
							// Sinon il tente de jouer alors que le jeu n'a pas commence
							out.println("-1");
							out.flush();
			
						}else if(panneau.getEtatJeu() == 2) {
							// Le jeu a demarre
							if(listeInfos[0].equals("Deplacement")) {
								// l'utilisateur vient de la page manette.jsp
								System.out.println("DEPLACEMENT : numJoueur : " +  listeInfos[1] + " depl " +  listeInfos[2]);
								int nbPieces = panneau.getCarte().deplacement(Integer.parseInt(listeInfos[1]), listeInfos[2]);
								out.println(nbPieces + "");
								out.flush();
							}
							// Sinon il tente de s'inscrire alors que le jeu a deja commence
							out.println("InscriptionErreur");
							out.flush();
						}else {
							out.println("-1");
							out.flush();
						}
									
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
				}
				server.close();

			}catch(IOException e) {}
		}

}