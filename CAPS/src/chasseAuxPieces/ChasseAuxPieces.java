package chasseAuxPieces;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.print.DocFlavor.URL;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.Timer;

import com.sun.tools.javac.Main;

/**
 * Programme principal Cree la fenetre graphique et gere les evenements
 *
 */
public class ChasseAuxPieces {
	static Panneau p = new Panneau();;

	public static void main(String[] args) {

		/*** ajouter bande son ***/

		try {
			java.net.URL url = Main.class.getClassLoader().getResource("son.wav");
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
			Clip clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
		} catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
			throw new RuntimeException(e);
		}

		/**** Interface graphique ****/
		new Thread() {
			public void run() {

				// Necessaire pour mettre en plein ecran
				GraphicsDevice device;
				device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

				if (!device.isFullScreenSupported()) {
					System.out.println("Plein ecran non supporte");
					System.exit(0);
				}

				// Creer la JFrame
				System.out.println("Creer et configurer la JFrame");
				JFrame monCadre = new JFrame("ChasseAuxPieces");
				monCadre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				// Creer et configurer la JMenuBar
				System.out.println("Creer et configurer le JMenuBar");
				JMenuBar menuBar = new JMenuBar();
				menuBar.setOpaque(true);
				menuBar.setBackground(new Color(255, 255, 238));
				menuBar.setPreferredSize(new Dimension(10, IConfig.NB_PIX_CASE));

				// Modifier le JPanel par defaut
				System.out.println("Configurer le JPanel");

				monCadre.getContentPane().setPreferredSize(new Dimension(IConfig.LARGEUR_CARTE * IConfig.NB_PIX_CASE,
						IConfig.HAUTEUR_CARTE * IConfig.NB_PIX_CASE));
				monCadre.setResizable(false);

				// Ajout du JMenuBar dans la frame
				System.out.println("Ajout du JMenuBar");
				monCadre.setJMenuBar(menuBar);

				System.out.println("Ajout du panneau au ContentPane");
				monCadre.getContentPane().add(p);

				// Affichage du chrono
				JLabel chrono;

				chrono = new JLabel("");
				chrono.setFont(IConfig.FONT);
				chrono.setBounds(300, 230, 200, 100);
				chrono.setHorizontalAlignment(JLabel.CENTER);

				menuBar.add(chrono);

				menuBar.setVisible(true);

				JButton boutonJouer;
				boutonJouer = new JButton("Jouer");

				// Changer la couleur du texte dans le bouton
				boutonJouer.setForeground(new Color(255, 255, 255));
				boutonJouer.setFont(IConfig.FONT);
				// Modifier la couleur de fond du bouton
				boutonJouer.setBackground(new Color(130, 154, 67));
				menuBar.add(boutonJouer);

				monCadre.setUndecorated(true);
				// Mettre en plein ecran
				device.setFullScreenWindow(monCadre);

				// Chronometre
				Timer timer = new Timer(1000, new ActionListener() {
					int seconde = 0, minute = 2;

					@Override
					public void actionPerformed(ActionEvent e) {

						String ddSecond, ddMinute;

						DecimalFormat dFormat = new DecimalFormat("00");
						seconde--;
						ddSecond = dFormat.format(seconde);
						ddMinute = dFormat.format(minute);
						chrono.setText("<html><font color = #0F3F0C>" + minute + ":" + ddSecond + "</font>" + "&emsp;"
								+ p.getCarte().getClassementProvisoire() + "</html>");

						if (seconde == -1) {
							seconde = 59;
							minute--;

							ddSecond = dFormat.format(seconde);
							ddMinute = dFormat.format(minute);
							chrono.setText("<html><font color = #0F3F0C>" + minute + ":" + ddSecond + "</font>"
									+ "&emsp;" + p.getCarte().getClassementProvisoire() + "</html>");

						}
						if (minute == 0 && seconde == 0) {
							chrono.setText("Fin");
							// Changer etat du jeu (fin du jeu)
							p.setEtatJeu(3);
							p.getCarte().trierListePersonnages();
						}
						if (minute < 0) {
							chrono.setText("Classement");
						}
					}
				});

				// Timer pour le rafraichissement de l'animation (toutes les 10 ms)
				Timer timerRepaint = new Timer(10, new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						p.repaint();
						// Obtenir une animation fluide sous Linux
						Toolkit.getDefaultToolkit().sync();

					}
				});

				// Lancer le timer pour le rafraichissement de l'affichage
				timerRepaint.start();

				/**************** Gestion des evenements ****************************/

				boutonJouer.addActionListener(new ActionListener() {
					// CLique sur le bouton Jouer
					public void actionPerformed(ActionEvent e) {
						p.getCarte().initPieces();
						// Rendre le bouton invisible
						boutonJouer.setVisible(false);
						// Changer la valeur de de l'etat du jeu
						p.setEtatJeu(2);

						// Lancer le chrono
						timer.start();

					}
				});
				/***********************************************/

			}

		}.start();

		/**** Serveur en attente de clients ****/
		new Thread() {
			public void run() {
				Serveur serveur = new Serveur(p);
				serveur.serveurSocket();
			}
		}.start();
	}

}