package chasseAuxPieces;

import java.awt.Graphics;


/**
 * Implementation d'une Piece
 *
 */
public class Piece extends Element {
	
	// Valeur de la piece
	int valeur;
	
	/*********** Constructeur d'une Piece ***************/
	/**
	 * Constructeur d'une Piece
	 * 
	 * @param pos
	 * 			la position de la Piece
	 * @param valeur
	 * 			la valeur de la Piece
	 */
	public Piece(Position pos, int valeur) {
		super(pos);
		this.valeur = valeur;
	}
	
	/***********************************************/
	
	/*********** Accesseurs ***************/
	
	public int getValeur() {
		return valeur;
	}
	
	/***********************************************/
	
	/*********** Methodes publiques ***************/
	/**
	 * Redefinition de toString
	 */
	 public String toString() {
		return "" + getPosition() + "Piece";
	}
	 
	/**
	 * Affiche une Piece
	 * 
	 * @param g
	 * 			le Graphics
	 */
	public void seDessiner(Graphics g) {
		if(valeur==5) {
	      g.setColor(IConfig.COULEUR_PIECE_OR);  
		}
		else if(valeur==3) {
			g.setColor(IConfig.COULEUR_PIECE_ARGENT); 
		}
		else if(valeur==1) {
			g.setColor(IConfig.COULEUR_PIECE_BRONZE); 
		}
	        //g.fillRect(getPosition().getX()*IConfig.NB_PIX_CASE, getPosition().getY()*IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE);
	        g.fillOval(getPosition().getX()*IConfig.NB_PIX_CASE, getPosition().getY()*IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE);
		
	}
	/***********************************************/
}
