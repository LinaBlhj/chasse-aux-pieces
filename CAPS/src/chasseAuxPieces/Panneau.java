package chasseAuxPieces;

import java.awt.Graphics;

import javax.swing.JPanel;
import java.io.Serializable;

/**
 * Cree le jeu et le dessine 
 *
 */
public class Panneau extends JPanel implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Carte carte;
	/* Indique l'etat du jeu
	 * 1 : phase d'inscription
	 * 2 : jeu
	 * 3 : fin du jeu
	 */
	private int etatJeu;
	
	/*********** Constructeur de Panneau ***************/
	/**
	 * Constructeur de Panneau
	 */
	public Panneau(){
		carte = new Carte() ;
		setBackground(IConfig.COULEUR_VIDE);
		etatJeu = 1;
		}
	
	
	/***********************************************/
	
	/*********** Accesseurs ***************/
	/*
	 * Renvoie la Carte
	 * @return
	 * 		la Carte
	 */
	public Carte getCarte() {
		return carte;
	}
	/***********************************************/
	
	/*********** Accesseurs ***************/
	public int getEtatJeu() {
		return etatJeu;
	}
	/***********************************************/
	
	/*********** Mutateur de Panneau ***************/
	/*
	 * Modifie la valeur de etatJeu
	 */
	public void setEtatJeu(int valeur) {
		etatJeu = valeur;
	}
	/***********************************************/
	
	
	/*********** Methodes publiques ***************/
	
	public void paintComponent(Graphics g) {
		// Rafraichir le background
		super.paintComponent(g);
		
		if(etatJeu == 1) {
			// Phase d'inscription
			carte.toutDessiner(g);
	
		}else if(etatJeu == 2) {
			// Jeu
			carte.toutDessiner(g);
		}else {
			// Fin du jeu
			carte.dessinerFinJeu(g);
		}
		//System.out.println("********repaint**********");
	}

	/***********************************************/
	
}