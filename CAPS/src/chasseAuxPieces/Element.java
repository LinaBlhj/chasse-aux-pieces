package chasseAuxPieces;

import java.awt.Graphics;

/**
 * Implementation de IElement
 *
 */
public abstract class Element {
	
	// position d'un element sur la carte
	private Position pos;
		
	/*********** Constructeur d'un Element ***************/
	
	/**
	 * Constructeur d'un Element
	 * 
	 * @param pos
	 * 			la position de l'Element
	 */
	public Element(Position pos) {
		this.pos = pos;
		  		  
	}
	/***********************************************/
	
	/*********** Accesseurs ***************/
	/**
	 * Position d'un Element
	 * 
	 * @return
	 * 		la position de l'Element
	 */
	public Position getPosition() {
		return new Position(pos.getX(), pos.getY());
	}
	

	/***********************************************/
	
	/*********** Mutateurs ***************/

	/**
	 * Mutateur de position
	 * 
	 * @param position
	 * 		nouvelle position
	 */
	public void setPosition(Position position) {
		pos = new Position(position.getX(), position.getY());
	}
	/***********************************************/
	
	/*********** Methodes abstraites ***************/
	/**
	 * Dessine l'Element
	 * 
	 * @param g
	 * 		Graphics
	 */
	public abstract void seDessiner(Graphics g);
	
	/***********************************************/
}