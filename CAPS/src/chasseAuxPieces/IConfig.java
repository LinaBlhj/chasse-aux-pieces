package chasseAuxPieces;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

/**
 * Configuration du jeu.
 *
 */
public interface IConfig {
	// Nombre de cases en largeur. On fixe cette valeur puis on calcule les autres
	int LARGEUR_CARTE = 35;
	int LARGEUR_CARTE1 = 45;//pour la barre de menu
	
	// Recuperer les dimensions de l'ecran
	Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize();
	// Nombre de pixels d'une case
	int NB_PIX_CASE = tailleEcran.width/IConfig.LARGEUR_CARTE;
	//pour la barre de menu
	int NB_PIX_CASE1 = tailleEcran.width/IConfig.LARGEUR_CARTE1;
	// Nombre de cases en hauteur. -1 pour prevoir l'affichage du chrono
	int HAUTEUR_CARTE = tailleEcran.height/NB_PIX_CASE - 2;
	
	int NB_OBSTACLES = 20;
	
	Color COULEUR_VIDE = new Color(130, 154, 67), COULEUR_INCONNU = new Color(98, 109, 70);
	Color COULEUR_TEXTE = Color.white;
	Color COULEUR_TEXTE_NOIR = Color.black;
	Color COULEUR_PERSONNAGE =  new Color(49,49,49);
	Color COULEUR_PIECE_OR =  new Color(255,255,0);	
	Color COULEUR_PIECE_ARGENT =  new Color(206, 206, 206);
	Color COULEUR_PIECE_BRONZE =  new Color(132, 46, 27);
	Color COULEUR_OBSTACLE = new Color(150,150,150);
	
	Font FONT = new Font("Georgia", Font.PLAIN, 20);
	Font FONT2 = new Font("Arial", Font.PLAIN, 40);
	Font FONT3 = new Font("Georgia", Font.PLAIN, 15);
}
