package chasseAuxPieces;

/**
 * Implementation IPosition  
 */
public class Position{
	
	private int x, y;
	
	/*********** Constructeur d'une Position ***************/
	/**
	 * Construction d'une position
	 * 
	 * @param x
	 * 			abscisse de la position
	 * @param y
	 * 			ordonnee de la position
	 */
	public Position(int x, int y){ 
		//System.out.println("Position.Position : Construction position (" + x + "," + y + ")");

		// Initialisation de la position
		this.setX(x); 
		this.setY(y);
	}
	/***********************************************/
	
	/*********** Accesseurs ***************/
	
	/**
	 * Accesseur de l'abscisse
	 * 
	 * @return
	 * 			abscisse
	 */
	public int getX() { return x; }
	
	/**
	 * Accesseur de l'ordonnee
	 * 
	 * @return
	 * 			ordonnee
	 */
	public int getY() { return y; }
	
	/***********************************************/
	
	/*********** Mutateurs ***************/
	
	/**
	 * Mutateur de l'abscisse
	 * 
	 * @param x
	 * 			nouvelle abscisse
	 */
	public void setX(int x) { 
		//System.out.println("Position.setX : Modification de l'abscisse (" + this.x + "->" + x +")");
		// Test parametre d'entree
		if(x>=0 && x< IConfig.LARGEUR_CARTE) {
			this.x = x; 
		}else {
			throw new IllegalArgumentException("L'abscisse n'est pas valide !");
		}
				
	}
	
	/**
	 * Mutateur de l'ordonnee
	 * 
	 * @param y
	 * 			nouvelle ordonnee
	 */
	public void setY(int y) { 
		//System.out.println("Position.setY : Modification de l'ordonnee (" + this.y + "->" + y +")");
		if(y>=0 && y< IConfig.HAUTEUR_CARTE) {
			this.y = y; 
		}else {
			throw new IllegalArgumentException("L'ordonnee n'est pas valide !");
		}	
	}
	/***********************************************/
	
	/*********** Methodes publiques ***************/
	
	/**
	 * Redefinition de toString
	 * 
	 * @return
	 * 			chaine de caractere
	 */
	public String toString() { return "("+x+","+y+")"; }
	
	/**
	 * Test si la position est voisine d'une autre position. 
	 * 
	 * @param pos
	 *			position a tester
	 * @return 
	 * 			true si pos est voisine, false sinon
	 */
	public boolean estVoisine(Position pos) {
		System.out.println("Position.estVoisine : Test si la position est voisine d'une autre position");
		
		// Test parametre d'entree
		if(pos == null) {
			throw new IllegalArgumentException("La position a tester n'est pas valide !");
		}
		
		return ((Math.abs(x-pos.getX())<=1) && (Math.abs(y-pos.getY())<=1));
	}
	/***********************************************/	
}

