package chasseAuxPieces;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;


/**
 * Implementation d'un Personnage
 *
 */
public class Personnage extends Element {
	
	// Pseudo du Personnage
	private String pseudo;
	// Numero du Personnage
	private int numero;
	// Nombre de pi�ces
	private int nbPieces;
	private int r;
	private int g;
	private int b;
	/*********** Constructeur d'un Personnage ***************/
	/**
	 * Constructeur d'un Personnage
	 * 
	 * @param pos
	 * 			la position du Personnage
	 * @param valeur
	 * 			la valeur du Personnage
	 */
	public Personnage(Position pos, String pseudo, int numero) {
		super(pos);
		this.pseudo = pseudo;
		this.numero = numero;
		do {
		r=(int) (Math.random()*255);
		g=(int) (Math.random()*255);
		b=(int) (Math.random()*255);
		}while((r>220 && g>220 && b>220) && (r<115 && r>140 && g!=154 && b!=67));
		nbPieces = 0;
	}
	
	public Personnage(String pseudo, int numero) {
		super(new Position(0, 0));
		this.pseudo = pseudo;
		this.numero = numero;
		nbPieces = 0;		
	}
	
	/***********************************************/
	
	/*********** Accesseurs ***************/
	public  String getPseudo() {
	    return pseudo;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public int getNombrePieces() {
		return nbPieces;
	}
	
	public String getCouleur() {
		return ""+r+" "+g+" "+b;
	}
	
	public void gainPieces(int nbPieces) {
		this.nbPieces+=nbPieces;
	}
	
	/***********************************************/
	
	/*********** Methodes publiques ***************/
	/**
	 * Redefinition de toString
	 */
	 public String toString() {
		return "" + getPosition() + " " + pseudo + " (" + numero + ")" + "nb pieces : " + nbPieces;
	}
	 
	/**
	 * Affiche un Personnage
	 * 
	 * @param g
	 * 			le Graphics
	 */
	public void seDessiner(Graphics g) {
		//g.setColor(IConfig.COULEUR_PERSONNAGE);		

		g.setColor(new Color(r,this.g,b));	
		//g.fillRect(getPosition().getX()*IConfig.NB_PIX_CASE, getPosition().getY()*IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE);
		g.fillOval(getPosition().getX()*IConfig.NB_PIX_CASE, getPosition().getY()*IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE, IConfig.NB_PIX_CASE);
		
	}
	
	public void numDessin(Graphics g) {
        String text=String.valueOf(getNumero());
        g.setColor(IConfig.COULEUR_TEXTE);
        g.setFont(IConfig.FONT3);
        g.drawString(text,getPosition().getX()*IConfig.NB_PIX_CASE + 3, getPosition().getY()*IConfig.NB_PIX_CASE+IConfig.NB_PIX_CASE/2 + 5);
    }
	/***********************************************/
}