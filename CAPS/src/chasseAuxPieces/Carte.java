package chasseAuxPieces;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class Carte {
	
	ArrayList<Personnage> listeTriee;
	private Element[][] carte;
	private ArrayList<Personnage> listePersonnages;
	private ArrayList<Piece> listePieces;
	private ArrayList<Obstacle> listeObstacles;
	
	/*********** Constructeur de la Carte ***************/
	/**
	 * Constructeur de la Carte
	 */
	public Carte() {
		carte=new Element[IConfig.HAUTEUR_CARTE][IConfig.LARGEUR_CARTE];
		listePersonnages = new ArrayList<Personnage>();
		listePieces=new ArrayList<Piece>();
		listeObstacles= new ArrayList<Obstacle>();
		
		/*Positionnement des pieces*/
		
		int i,j,v,valeur;
		/*int nbPieces = 0;
		Piece p;
		while(nbPieces < 5) {
			System.out.println("ok");
			i=(int) (Math.random()*(IConfig.HAUTEUR_CARTE));
			j=(int) (Math.random()*(IConfig.LARGEUR_CARTE));
			valeur=(int) (Math.random()*(10-1)) + 1;
			
			if(carte[i][j]==null) {
				if(valeur<=5) {
					p = new Piece(new Position(j,i),1);
				}
				else if(valeur<=9) {
					p = new Piece(new Position(j,i),3);
				}
				else {
					p = new Piece(new Position(j,i),5);
				}
				carte[i][j]=p;
				listePieces.add(p);
				nbPieces++;
			}
		}
					*/
			/*afficher obstacles*/
			
			for(v=0;v<10;v++) {
				
				i=(int) (Math.random()*(IConfig.HAUTEUR_CARTE));
				j=(int) (Math.random()*(IConfig.LARGEUR_CARTE));
				
				if(carte[i][j]==null) {
					carte[i][j]=new Obstacle(new Position(j,i));
					listeObstacles.add(new Obstacle(new Position(j,i)));
				}
			
			}
		
		 
		}
	
	/***********************************************/
	
	
	/*********** Mutateurs ***************/
	
	/**Cree la liste des pieces
	 * 
	 */
	
	public void initPieces() {
		int i,j,valeur;
		int nbPieces = 0;
		Piece p;
		while(nbPieces < listePersonnages.size() * 0.70) {
			System.out.println("ok");
			i=(int) (Math.random()*(IConfig.HAUTEUR_CARTE));
			j=(int) (Math.random()*(IConfig.LARGEUR_CARTE));
			valeur=(int) (Math.random()*10)+1;
			System.out.println(valeur);
			if(carte[i][j]==null) {
				if(valeur<=5) {
					p = new Piece(new Position(j,i),1);
				}
				else if(valeur<=9) {
					p = new Piece(new Position(j,i),3);
				}
				else {
					p = new Piece(new Position(j,i),5);
				}
				carte[i][j]=p;
				listePieces.add(p);
				nbPieces++;
			}
		}
	}
	
	/**
	 * Ajoute un personnage a la liste
	 * 
	 * @param pseudo	
	 * 			le pseudo du pesonnage
	 * @return
	 * 			le numero du personnage
	 */
	public int ajouterPersonnage(String pseudo) {

		int i,j;
		Position p;
		do {
			i=(int)(Math.random()*(IConfig.HAUTEUR_CARTE));
			j=(int)(Math.random()*(IConfig.LARGEUR_CARTE));		
		}while(carte[i][j]!=null); 

		p= new Position(j,i);
		int numero = listePersonnages.size() + 1;
		Personnage perso = new Personnage(p, pseudo, numero);
		listePersonnages.add(perso);
		carte[i][j] = perso;
		return numero;
	}
	
	
	/***********************************************/
	
	/*********** Methodes publiques ***************/
	/**
	 * Dessine le plateau de jeu
	 * 	 
	 * @param g
	 * 			Graphics
	 */
	public void toutDessiner(Graphics g) {
		// Dessiner la grille en dernier 
		dessinerGrille(g);
		for(Personnage p : listePersonnages) {
			p.seDessiner(g);
			p.numDessin(g);
		}
		
		for(Piece piece : listePieces) {
			piece.seDessiner(g);
		}
		
		for(Obstacle obstacle: listeObstacles) {
			obstacle.seDessiner(g);
		}
	}
	
	/*
	 * Dessine les pseudos des joueurs
	 * @param g
	 * 		Graphics
	 */
	public void dessinerPseudos(Graphics g) {
		int x=10;
		int y = IConfig.NB_PIX_CASE1/2;
		g.setColor(IConfig.COULEUR_TEXTE);
		g.setFont(IConfig.FONT); 
	
		for(Personnage joueur : listePersonnages) {
		
			g.drawString("\n\n\n\n\n"+joueur.getNumero() + " " + joueur.getPseudo(), x, y);
			
			y = y + IConfig.NB_PIX_CASE1;
		
		if((joueur.getNumero()==30)||(joueur.getNumero()==60)||(joueur.getNumero()==90)||(joueur.getNumero()==120)||(joueur.getNumero()==150)||(joueur.getNumero()==180)) {
			x=x+200;
			y=IConfig.NB_PIX_CASE1/2;
		}
		}
	}
	
	public void dessinerFinJeu(Graphics g) {
		int x=10;
		int y = IConfig.NB_PIX_CASE1/2;
		g.setFont(IConfig.FONT); 

		int position = 1;
		
		for(Personnage joueur : listeTriee) {
			
			g.setColor(IConfig.COULEUR_TEXTE_NOIR);
			g.drawString("\n\n\n\n\n"+position+ "  [ " + joueur.getNombrePieces() + " Pieces] "+ joueur.getPseudo(), x, y);
			y = y + IConfig.NB_PIX_CASE1;
			position++;
			
			if((position==30)||(position==60)||(position==90)||(position==120)) {
				x=x+300;
				y=IConfig.NB_PIX_CASE1/2;
			}
		}

	}
	/**
	 * Place aleatoirement une piece sur la carte
	 * @return
	 * 		la position
	 */
	public Position nouvellePiece(){
		int i,j,valeur;
		Position p;
		do {
			i=(int)(Math.random()*(IConfig.HAUTEUR_CARTE));
			j=(int)(Math.random()*(IConfig.LARGEUR_CARTE));
			p= new Position(j,i);
		}while(carte[i][j]!=null); 
		
		valeur=(int) (Math.random()*10) + 1;
		System.out.println(valeur);
		if(valeur<=5) {
			carte[i][j]=new Piece(p,1);
		}
		else if(valeur<=9) {
			carte[i][j]=new Piece(p,3);
		}
		else {
			carte[i][j]=new Piece(p,5);
		}
		
		listePieces.add((Piece) carte[i][j]);
		return p;		
	}
	
	/**
	 * Gestion des deplacements des Personnages sur la Carte
	 * 
	 * @param numeroPerso
	 * 			numero du Personnage a deplacer
	 * @param valeurDeplacement
	 * 			direction du Personage (H : Haut, B : Bas, D : Droite, G : Gauche)
	 * @return
	 * 			le nombre de Pieces du Personnage s'il a change, -1 si inchange
	 */
	public int deplacement(int numeroPerso, String valeurDeplacement) {
		
		Personnage perso=listePersonnages.get(numeroPerso-1);
		int x=perso.getPosition().getX();
		int y=perso.getPosition().getY();
		switch(valeurDeplacement) {
		case "H": 
			if(y-1>=0) {

				if(carte[y-1][x]==null) {
					System.out.println("ok1");
					perso.setPosition(new Position(x,y-1));	
					carte[y][x]=null;
					carte[y-1][x]=perso;
				}
				else if(carte[y-1][x] instanceof Piece) {
					perso.gainPieces(((Piece) (carte[y-1][x])).getValeur());
					listePieces.remove((Piece) carte[y-1][x]);
					carte[y][x]=null;
					perso.setPosition(new Position(x,y-1));	
					carte[y-1][x]=perso;
					nouvellePiece();
					return perso.getNombrePieces();
				}
				
			}
			break;
			
		case "B":
			if(y+1<IConfig.HAUTEUR_CARTE) {
				if(carte[y+1][x]==null) {
					System.out.println("ok2");
					perso.setPosition(new Position(x,y+1));	
					carte[y][x]=null;
					carte[y+1][x]=perso;
				}
				else if(carte[y+1][x] instanceof Piece) {
					perso.gainPieces(((Piece) (carte[y+1][x])).getValeur());
					listePieces.remove((Piece) carte[y+1][x]);
					carte[y][x]=null;
					perso.setPosition(new Position(x,y+1));	
					carte[y+1][x]=perso;
					nouvellePiece();
					return perso.getNombrePieces();
				}
			}
			break;
			
		case "G":
			if(x-1>=0){
				if(carte[y][x-1]==null) {
					System.out.println("ok3");
					perso.setPosition(new Position(x-1,y));	
					carte[y][x]=null;
					carte[y][x-1]=perso;
				}
				else if(carte[y][x-1] instanceof Piece) {
					perso.gainPieces(((Piece) (carte[y][x-1])).getValeur());
					listePieces.remove(carte[y][x-1]);
					carte[y][x]=null;
					perso.setPosition(new Position(x-1,y));	
					carte[y][x-1]=perso;
					nouvellePiece();
					return perso.getNombrePieces();
				}
			}
			break;
			
		case "D":
			if(x+1<IConfig.LARGEUR_CARTE) {
				if(carte[y][x+1]==null) {
					System.out.println("ok4");
					perso.setPosition(new Position(x+1,y));	
					carte[y][x]=null;
					carte[y][x+1]=perso;
				}
				else if(carte[y][x+1] instanceof Piece) {
					perso.gainPieces(((Piece) (carte[y][x+1])).getValeur());
					listePieces.remove(carte[y][x+1]);
					carte[y][x]=null;
					perso.setPosition(new Position(x+1,y));	
					carte[y][x+1]=perso;
					nouvellePiece();
					return perso.getNombrePieces();
				}
			}
			break;
		}
		System.out.println(perso.getPosition());
		System.out.println("Score: "+perso.getNombrePieces());
		return -1;
	}

	
	/**
	 * Trie listePersonnages selon le nombre de pieces et met le resultat dans listeTriee
	 */
	public void trierListePersonnages() {
		// Copier le contenu de listePersonnages dans listeTrie
		listeTriee = new ArrayList<Personnage>(listePersonnages);
		listeTriee.sort(Comparator.comparing(Personnage::getNombrePieces).reversed());
	}
	
	/**
	 * Classe les personnages selon leur nombre de pieces
	 * @return
	 * 			la chaine de caractere contenant les 3 meilleurs joueurs
	 */
	public String getClassementProvisoire() {
		String classement = "";
		int i = 1;
		
		// Copier le contenu de listePersonnages dans listeTrie
		listeTriee = new ArrayList<Personnage>(listePersonnages);
		listeTriee.sort(Comparator.comparing(Personnage::getNombrePieces).reversed());
		
		while(i < 4 && listeTriee.size() > i - 1) {
			classement = classement + "   <font color = #B75C66>[" + i + "]</font> " + listeTriee.get(i-1).getPseudo() + "  <font color = #799279>" + listeTriee.get(i-1).getNombrePieces();
			if( listeTriee.get(i-1).getNombrePieces() < 2) {
				classement= classement + " Piece</font>";
			}else {
				classement= classement + " Pieces</font>";
			}
			i++;
		}
		
		return classement;	
	}

	public ArrayList<Personnage> getListePersonnages(){
		return listePersonnages;
	}
	/***********************************************/
		
	
	/*********** Methodes privees ***************/
	
	/**
	 * Dessine la grille du jeu
	 * 
	 * @param g
	 * 			Graphics
	 */
	private void dessinerGrille(Graphics g) {
		g.setColor(new Color(180, 204, 17));
		// Tracer les lignes
		for(int i = 1; i <= IConfig.HAUTEUR_CARTE; i++) {
			g.drawLine(0, i*IConfig.NB_PIX_CASE, IConfig.LARGEUR_CARTE*IConfig.NB_PIX_CASE, i*IConfig.NB_PIX_CASE);
		}
		
		// Tracer les colonnes
		for(int j = 0; j <= IConfig.LARGEUR_CARTE; j++) {
			g.drawLine(j*IConfig.NB_PIX_CASE, 0, j*IConfig.NB_PIX_CASE, IConfig.HAUTEUR_CARTE*IConfig.NB_PIX_CASE);
		}
	}
	/***********************************************/
}